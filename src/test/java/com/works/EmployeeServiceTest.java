package com.works;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.works.dao.EmployeeRepositoryDao;
import com.works.model.Employee;
import com.works.service.EmployeeService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EmployeeServiceTest {

	@Autowired
	private EmployeeService employeeService;

	@MockBean
	private EmployeeRepositoryDao employeeRepositoryDao;

	@Test
	public void testSaveEmployee() {

		Employee employee = new Employee();
		employee.setId(1);
		employee.setFirstName("tuhin");
		employee.setLastName("mandal");
		employee.setEmail("tuhin@gmail.com");
		Mockito.when(employeeRepositoryDao.save(employee)).thenReturn(employee);

		assertThat(employeeService.saveEmployee(employee)).isEqualTo(employee);
	}

	@Test
	public void testGetEmployeeById() {
		Employee employee = new Employee();
		employee.setId(1);
		employee.setFirstName("tuhin");
		employee.setLastName("mandal");
		employee.setEmail("tuhin@gmail.com");

		Mockito.when(employeeRepositoryDao.findById(1)).thenReturn(employee);
		assertThat(employeeService.getEmployeeById(1)).isEqualTo(employee);
	}
@Test
	public void testGetAllEmployees(){
		Employee employee = new Employee();
		employee.setFirstName("tuhin");
		employee.setLastName("mandal");
		employee.setEmail("tuhin@gmail.com");
		
		Employee employee2 = new Employee();
		employee2.setFirstName("papu");
		employee2.setLastName("mandal");
		employee2.setEmail("papu@gmail.com");
		
		List<Employee> employeeList = new ArrayList<>();
		employeeList.add(employee);
		employeeList.add(employee2);
		
		Mockito.when(employeeRepositoryDao.findAll()).thenReturn(employeeList);
		
		assertThat(employeeService.getAllEmployees()).isEqualTo(employeeList);
	}
	
	
	@Test
	public void testUpdateEmployee(){
		Employee employee = new Employee();
		employee.setId(1);
		employee.setFirstName("tuhin");
		employee.setLastName("mandal");
		employee.setEmail("papu@gmail.com");
		
		Mockito.when(employeeRepositoryDao.findById(1)).thenReturn(employee);
		
		employee.setEmail("kalyan@gmail.com");
		Mockito.when(employeeRepositoryDao.save(employee)).thenReturn(employee);
		
		assertThat(employeeService.updateEmployee(1)).isEqualTo(employee);
		
	}
	
	
	
}
