package com.works.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.works.dao.EmployeeRepositoryDao;
import com.works.model.Employee;

@Service
public class EmployeeService {

	@Autowired
	private EmployeeRepositoryDao employeeRepositoryDao;

	public Employee getEmployeeById(int id) {
		return employeeRepositoryDao.findById(id);
	}

	public Employee saveEmployee(Employee employee) {
		return employeeRepositoryDao.save(employee);
	}

	public List<Employee> getAllEmployees() {
		return (List<Employee>) employeeRepositoryDao.findAll();
	}
	public Employee updateEmployee(int empId) {
		Employee empFromDb = employeeRepositoryDao.findById(empId);
		Employee updatedEmployee = employeeRepositoryDao.save(empFromDb);
		return updatedEmployee;
	}
}
