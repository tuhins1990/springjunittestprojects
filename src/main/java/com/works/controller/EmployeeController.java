package com.works.controller;

import java.net.URI;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.works.dao.EmployeeRepositoryDao;
import com.works.model.Employee;
import com.works.model.Employees;
import com.works.service.EmployeeService;

@RestController
public class EmployeeController {

	@Autowired
	private EmployeeRepositoryDao employeeRepository;

	@Autowired
	private EmployeeService employeeService;

	@GetMapping(path = "/employees", produces = "application/json")
	public Employees getAllEmployees() {
		Employees response = new Employees();
		ArrayList<Employee> list = new ArrayList<>();
		employeeRepository.findAll().forEach(e -> list.add(e));
		response.setEmployeeList(list);
		return response;
	}

	@PostMapping(path = "/save/employees", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> addEmployee(@RequestBody Employee employee) {

		employee = employeeRepository.save(employee);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(employee.getId())
				.toUri();

		return ResponseEntity.created(location).build();
	}

	@GetMapping("/employees/{id}")
	public Employee getEmployeeById(@PathVariable int id) {

		return employeeService.getEmployeeById(id);

	}

	// @CrossOrigin
	@DeleteMapping(value = "/employees/{id}", consumes = "application/json", produces = "application/json")
	public void deleteTicket(@PathVariable("id") int id) {
		Employee employee = employeeRepository.findById(id);
		employeeRepository.delete(employee);
	}

	// @CrossOrigin
	@PutMapping(value = "/employees/{id}", consumes = "application/json", produces = "application/json")
	public Employee updateEmployee(@PathVariable(value="id") int id,@RequestBody Employee employeeDetails) {
		Employee employee = employeeRepository.findById(id);
        employee.setFirstName(employeeDetails.getFirstName());
        employee.setLastName(employeeDetails.getLastName());
        employee.setEmail(employeeDetails.getEmail());
		return employeeRepository.save(employee);
	}
}
